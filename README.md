# fans-cloud-front

#### 项目介绍
fans-cloud-front 是一套基于nodejs vue iview后台管理系统前端快速开发模板化解决方案，几句代码即可快速开发属于你的页面。

## 技术栈
  vue2 + vuex + vue-router + webpack + ES6 + axios + iview

#### 后端代码地址
  https://gitee.com/fhh/fans-cloud-alibaba
#### springboot版本代码地址
 前端：https://gitee.com/fhh/fansbfront
 后端：https://gitee.com/fhh/fans-boot

#### 安装教程

#### 安装依赖
npm install

#### 运行 localhost:8000
npm run dev
登录用户名/密码：admin/123456

#### 打包
npm run build

#### 引用关系

![输入图片说明](https://images.gitee.com/uploads/images/2019/0910/105203_5b4f4e61_79358.png "主要引用关系.png")

#### 软件架构
![软件架构](https://images.gitee.com/uploads/images/2020/0524/172332_9470bce0_79358.png "软件架构.png")

#### 使用说明
安装依赖（npm install）时可能会出现node-sass 安装失败的问题，解决方法如下：
安装淘宝镜像源后使用 cnpm 安装 node-sass 会默认从淘宝镜像源下载：cnpm install node-sass

#### 运行效果
![登录](https://images.gitee.com/uploads/images/2020/0524/172651_cdf916ce_79358.png "登录.png")
![首页](https://images.gitee.com/uploads/images/2020/0524/174324_0fba81e7_79358.png "首页.png")
![用户管理](https://images.gitee.com/uploads/images/2020/0524/174651_7dc42de3_79358.png "用户管理.png")
![菜单管理](https://images.gitee.com/uploads/images/2020/0524/174750_21f8828e_79358.png "菜单管理.png")
![菜单图标配置](https://images.gitee.com/uploads/images/2020/0524/174920_73239bdc_79358.png "菜单图标配置.png")
![角色管理](https://images.gitee.com/uploads/images/2020/0524/174953_ba301f06_79358.png "角色管理.png")
![数据字典管理](https://images.gitee.com/uploads/images/2020/0524/175047_2ee44b71_79358.png "数据字典管理.png")
![字典项管理](https://images.gitee.com/uploads/images/2020/0524/175028_83bd3a2c_79358.png "字典项管理.png")
![操作日志](https://images.gitee.com/uploads/images/2020/0524/175130_24eb036f_79358.png "操作日志.png")
![登录日志](https://images.gitee.com/uploads/images/2020/0524/175151_129cb3e5_79358.png "登录日志.png")
![定时任务](https://images.gitee.com/uploads/images/2020/0524/175219_7cb0749c_79358.png "定时任务.png")
![代码生成](https://images.gitee.com/uploads/images/2020/0524/175312_e65e90ba_79358.png "代码生成.png")
![生成器生成的测试页面](https://images.gitee.com/uploads/images/2020/0524/175332_a3a09556_79358.png "生成器生成的测试页面.png")
![nacos管理](https://images.gitee.com/uploads/images/2020/0524/175415_fa3bef63_79358.png "nacos管理.png")
![sentinel控制台](https://images.gitee.com/uploads/images/2020/0524/175443_3eb17440_79358.png "sentinel控制台.png")
![skywalking首页](https://images.gitee.com/uploads/images/2020/0524/175528_c3e71843_79358.png "skywalking首页.png")
![skywalking网络追踪图](https://images.gitee.com/uploads/images/2020/0524/175548_53a6f4dd_79358.png "skywalking网络追踪图.png")
![skywalking网络拓扑图](https://images.gitee.com/uploads/images/2020/0524/175609_c0cb775b_79358.png "skywalking网络拓扑图.png")
![skywalking jvm监控](https://images.gitee.com/uploads/images/2020/0524/175644_8cd86064_79358.png "skywalkingjvm监控.png")
![接口doc文档（auth）](https://images.gitee.com/uploads/images/2020/0524/180229_c43267d9_79358.png "接口doc文档（auth）.png")
![接口doc文档（fans-admin）](https://images.gitee.com/uploads/images/2020/0524/180244_31472e0c_79358.png "接口doc文档（fans-admin）.png")
![swagger接口文档（fans-auth）](https://images.gitee.com/uploads/images/2020/0524/180313_c80d71c7_79358.png "swagger接口文档（fans-auth）.png")
![swagger接口文档（fans-admin）](https://images.gitee.com/uploads/images/2020/0524/180300_aca04333_79358.png "swagger接口文档（fans-admin）.png")

#### 参考网址
可以参考iview-admin：https://lison16.github.io/iview-admin-doc/#/

